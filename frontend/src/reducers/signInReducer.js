import * as signinConstants from '../constants/signIn';

const INITIAL_STATE = {
    email: undefined,
    password: undefined,
};

export default function signIn(state = INITIAL_STATE, action) {
    switch (action.type) {
    case signinConstants.SIGN_IN_REQUEST:
        return {
            ...state,
            email: action.payload.email,
            password: action.payload.password,
        };
    default:
        return state;
    }
}
