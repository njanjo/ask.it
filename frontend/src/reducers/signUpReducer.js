import * as signUpConstants from '../constants/signUp';

const INITIAL_STATE = {
    password: undefined,
    confirmPassword: undefined,
    email: undefined,
    username: undefined,
    firstName: undefined,
    lastName: undefined,
    errorMessage: '',
};

export default function signUp(state = INITIAL_STATE, action) {
    switch (action.type) {
    case signUpConstants.SIGN_UP_REQUEST:
        return {
            ...state,
        };
    case signUpConstants.SIGN_UP_SUCCESS:
        return {
            ...state,
        };
    case signUpConstants.SIGN_UP_FAILURE:
        return {
            ...state,
            signUpError: true,
            errorMessage: (action.payload && action.payload.error) || 'Unknown error',
        };
    default:
        return state;
    }
}
