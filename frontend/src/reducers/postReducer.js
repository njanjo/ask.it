import * as postConstants from '../constants/post';

const INITIAL_STATE = {
    fetchingPostAndReplies: true,
    fetchingPosts: true,
    replies: {},
    posts: {},
};

export default function signIn(state = INITIAL_STATE, action) {
    switch (action.type) {
    case postConstants.POST_REPLY_SUCCESS:
        return {
            ...state,
            ...action.payload.entities,
        };
    case postConstants.VOTE_REPLY_SUCCESS:
        return {
            ...state,
            replies: {
                ...state.replies,
                ...action.payload.entities.replies,
            },
        };
    case postConstants.VOTE_POST_SUCCESS:
        return {
            ...state,
            posts: {
                ...state.posts,
                ...action.payload.entities.posts,
            },
        };
    case postConstants.FETCH_USER_POSTS_REQUEST:
        return {
            ...state,
            fetchingPosts: true,
        };
    case postConstants.FETCH_USER_POSTS_FAILURE:
        return {
            ...state,
            fetchingPosts: false,
        };
    case postConstants.FETCH_USER_POSTS_SUCCESS:
        return {
            ...state,
            fetchingPosts: false,
            posts: {
                ...state.posts,
                ...action.payload.entities.posts,
            },
            hasMorePosts: action.payload.hasMorePosts,
        };

    case postConstants.FETCH_HOT_POSTS_REQUEST:
        return {
            ...state,
            fetchingPosts: true,
        };
    case postConstants.FETCH_HOT_POSTS_FAILURE:
        return {
            ...state,
            fetchingPosts: false,
        };
    case postConstants.FETCH_HOT_POSTS_SUCCESS:
        return {
            ...state,
            fetchingPosts: false,
            posts: {
                ...state.posts,
                ...action.payload.entities.posts,
            },
            hasMorePosts: action.payload.hasMorePosts,
        };
    case postConstants.FETCH_POST_AND_REPLIES_SUCCESS:
        return {
            ...state,
            replies: {},
            fetchingPostAndReplies: false,
            ...action.payload.entities,
        };
    case postConstants.FETCH_POST_AND_REPLIES_FAILURE:
        return {
            ...state,
            fetchingPostAndReplies: false,
        };
    case postConstants.FETCH_POST_AND_REPLIES_REQUEST:
        return {
            ...state,
            fetchingPostAndReplies: true,
        };
    default:
        return state;
    }
}
