import * as signinConstants from '../constants/signIn';

const INITIAL_STATE = {
    current: undefined,
    loggedIn: false,
    loggingIn: false,
};

export default function signIn(state = INITIAL_STATE, action) {
    switch (action.type) {
    case signinConstants.FETCH_ME_SUCCESS:
        return {
            ...state,
            current: action.payload,
            loggedIn: action.payload.id !== null && action.payload.id !== -1,
            loggingIn: false,
        };
    case signinConstants.FETCH_ME_REQUEST:
        return {
            ...state,
            loggingIn: true,
        };
    case signinConstants.FETCH_ME_FAILURE:
        return {
            ...state,
            loggedIn: false,
            loggingIn: false,
            current: {
                id: -1,
            },
        };
    default:
        return state;
    }
}
