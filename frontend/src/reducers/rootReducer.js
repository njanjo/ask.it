import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as form } from 'redux-form';
import user from './userReducer';
import post from './postReducer';
import signIn from './signInReducer';
import signUp from './signUpReducer';


export default history => combineReducers({
    router: connectRouter(history),
    form,
    user,
    post,
    signIn,
    signUp,
});
