import { createSelector } from 'reselect';

const getSignUpAsyncErrors = state => state.form.signUp && state.form.signUp.asyncErrors;
const getSignUpSyncErrors = state => state.form.signUp && state.form.signUp.syncErrors;
const getSignInSyncErrors = state => state.form.signIn && state.form.signIn.syncErrors;
const getCreatePostSyncErrors = state => state.form.createPost && state.form.createPost.syncErrors;

export const isSignUpFormValidSelector = createSelector(
    [getSignUpAsyncErrors, getSignUpSyncErrors],
    (asyncErrors, syncErrors) => !(asyncErrors || syncErrors)
);

export const isSignInFormValidSelector = createSelector(
    [getSignInSyncErrors],
    syncErrors => !syncErrors
);

export const isCreatePostFormValidSelector = createSelector(
    [getCreatePostSyncErrors],
    syncErrors => !syncErrors
);
export const signUpEmail = state => state.form.signUp.values.email;
export const signUpFirstName = state => state.form.signUp.values.firstName;
export const signUpLastName = state => state.form.signUp.values.lastName;
export const signUpPassword = state => state.form.signUp.values.password;

const getPosts = state => state.post.posts || {};

export const hotPostsAsArraySelector = createSelector(
    [getPosts],
    posts => Object.keys(posts).map(p => posts[p]).sort((a, b) => b.totalVotes - a.totalVotes)
);

const getUserId = state => state.user.current.id || {};


export const userPostsAsArraySelector = createSelector(
    [getPosts, getUserId],
    (posts, userId) => Object.keys(posts).map(p => posts[p]).filter(p => p.userId === userId).sort((a, b) => b.totalVotes - a.totalVotes)
);

const getReplies = state => state.post.replies || {};


export const postRepliesArraySelector = createSelector(
    [getReplies],
    replies => Object.keys(replies).map(r => replies[r]).sort((a, b) => b.totalVotes - a.totalVotes)
);


export const signInEmail = state => state.form.signIn.values.email;
export const signInPassword = state => state.form.signIn.values.password;


export const currentPosts = state => state.post.posts;
export const currentReplies = state => state.post.replies;

export const postReplyContent = state => state.form.postReply.values.reply;

export const createPostContent = state => state.form.createPost.values.content;
export const createPostTitle = state => state.form.createPost.values.title;
