import {
    all, takeEvery, call, select,
} from 'redux-saga/effects';
import api from '../helpers/api';
import * as signUpConstants from '../constants/signUp';
import BackendPaths from '../helpers/backendPaths';
import { signUpEmail } from './selectors';


function* checkEmail({ payload: { resolve, reject } }) {
    try {
        const email = yield select(signUpEmail);
        if (email === undefined) return;
        const url = `${BackendPaths.checkEmail.getPath({ email })}`;
        const result = yield call(api, url, 'GET');
        if (result.emailUsed) yield call(reject, { email: 'Email is already used!' });
        yield call(resolve); // form submit resolve
    } catch (error) {
        yield call(reject, { email: 'Error while checking email...' });
    }
}

function* watchCheckEmail() {
    yield takeEvery(signUpConstants.CHECK_EMAIL, checkEmail);
}


export default function* userSagas() {
    yield all([
        watchCheckEmail(),
    ]);
}
