import {
    all, takeEvery, call, put, select,
} from 'redux-saga/effects';
import { push } from 'connected-react-router';
import api from '../helpers/api';
import * as signInConstants from '../constants/signIn';
import * as signUpConstants from '../constants/signUp';
import BackendPaths from '../helpers/backendPaths';
import { isStorageAvailable } from '../helpers/util';
import routes from '../helpers/applicationRoutes';
import {
    signInEmail,
    signInPassword,
    signUpEmail,
    signUpFirstName,
    signUpLastName,
    signUpPassword,
} from './selectors';

const STORAGE = window.localStorage;
const IS_STORAGE_AVAILABLE = isStorageAvailable(STORAGE);

function* fetchMe() {
    try {
        const me = yield call(api, BackendPaths.whoami.getPath(), 'GET', {
            withCredentials: true,
        });
        yield put({ type: signInConstants.FETCH_ME_SUCCESS, payload: me });
    } catch (e) {
        yield put({ type: signInConstants.FETCH_ME_FAILURE });
    }
}

function* watchFetchMe() {
    yield takeEvery(signInConstants.FETCH_ME_REQUEST, fetchMe);
}

function* signIn() {
    try {
        const params = {
            body: {
                email: yield select(signInEmail),
                password: yield select(signInPassword),
            },
            storeCredentials: true,
        };
        yield call(api, BackendPaths.session.getPath(), 'POST', params);
        yield* fetchMe();
    } catch (e) {
        yield put({ type: signInConstants.SIGN_IN_FAILURE });
    }
}

function* watchSignIn() {
    yield takeEvery(signInConstants.SIGN_IN_REQUEST, signIn);
}

function* signOut() {
    try {
        yield call(api, BackendPaths.session.getPath(), 'DELETE', {
            withCredentials: true,
        });
        if (IS_STORAGE_AVAILABLE) {
            const authorization = window.localStorage.getItem('bearer-token');
            if (authorization) {
                window.localStorage.removeItem('bearer-token');
            }
        }
        yield* fetchMe();
        yield put(push(routes.root.path));
    } catch (e) {
        yield put({ type: signInConstants.SIGN_OUT_FAILURE });
    }
}

function* watchSignOut() {
    yield takeEvery(signInConstants.SIGN_OUT_REQUEST, signOut);
}

function* signUp() {
    try {
        const params = {
            body: {
                email: yield select(signUpEmail),
                firstName: yield select(signUpFirstName),
                lastName: yield select(signUpLastName),
                password: yield select(signUpPassword),
            },
            storeCredentials: true,
        };
        yield call(api, BackendPaths.user.getPath(), 'POST', params);
        yield put({ type: signUpConstants.SIGN_UP_SUCCESS });
        yield* fetchMe();
    } catch (error) {
        const body = yield error;
        yield put({ type: signUpConstants.SIGN_UP_FAILURE, payload: body });
    }
}

function* watchSignUp() {
    yield takeEvery(signUpConstants.SIGN_UP_REQUEST, signUp);
}


export default function* userSagas() {
    yield all([
        watchSignUp(),
        watchFetchMe(),
        watchSignIn(),
        watchSignOut(),
    ]);
}
