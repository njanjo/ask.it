import {
    all, takeEvery, call, put, select,
} from 'redux-saga/effects';
import { normalize } from 'normalizr';
import { push } from 'connected-react-router';
import api from '../helpers/api';
import * as postConstants from '../constants/post';
import BackendPaths from '../helpers/backendPaths';
import { post, reply, postList } from '../helpers/entities';
import {
    currentPosts, currentReplies, postReplyContent, createPostContent, createPostTitle,
} from './selectors';
import routes from '../helpers/applicationRoutes';

function* createPost() {
    try {
        const params = {
            body: {
                content: yield select(createPostContent),
                title: yield select(createPostTitle),
            },
            withCredentials: true,
        };
        const createdPost = yield call(api, `${BackendPaths.createPost.getPath()}`, 'POST', params);
        yield put({ type: postConstants.CREATE_NEW_POST_SUCCESS });
        yield put(push(routes.post.getPath({ id: createdPost.id })));
    } catch (e) {
        yield put({ type: postConstants.CREATE_NEW_POST_SUCCESS, payload: e });
    }
}

function* watchCreatePost() {
    yield takeEvery(postConstants.CREATE_NEW_POST_REQUEST, createPost);
}


function* postReply(event) {
    try {
        const params = {
            body: {
                postId: event.payload.postId,
                content: yield select(postReplyContent),
            },
            withCredentials: true,
        };
        const replyWithLikes = yield call(api, `${BackendPaths.postReply.getPath()}`, 'POST', params);

        const replies = yield select(currentReplies);
        const posts = yield select(currentPosts);
        const currentPost = posts[replyWithLikes.postId];
        const repliesAsArray = Object.keys(replies).map(r => replies[r]);
        repliesAsArray.push(replyWithLikes);
        currentPost.replies = repliesAsArray;
        const normalizedData = normalize(currentPost, post);
        yield put({ type: postConstants.POST_REPLY_SUCCESS, payload: { ...normalizedData } });
    } catch (e) {
        yield put({ type: postConstants.POST_REPLY_FAILURE, payload: e });
    }
}

function* watchPostReply() {
    yield takeEvery(postConstants.POST_REPLY_REQUEST, postReply);
}

function* voteReply(event) {
    try {
        const params = {
            body: {
                vote: event.payload.vote,
            },
            withCredentials: true,
        };
        const replyWithLikes = yield call(api, `${BackendPaths.voteReply.getPath({ id: event.payload.id })}`, 'POST', params);
        const replies = yield select(currentReplies);
        const currentReplyState = replies[replyWithLikes.id];
        const updatedReply = {
            ...currentReplyState,
            ...replyWithLikes,
        };
        const normalizedData = normalize(updatedReply, reply);
        yield put({ type: postConstants.VOTE_REPLY_SUCCESS, payload: { ...normalizedData } });
    } catch (e) {
        yield put({ type: postConstants.VOTE_REPLY_FAILURE, payload: e });
    }
}

function* watchVoteReply() {
    yield takeEvery(postConstants.VOTE_REPLY_REQUEST, voteReply);
}


function* votePost(event) {
    try {
        const params = {
            body: {
                vote: event.payload.vote,
            },
            withCredentials: true,
        };
        const postWithLikes = yield call(api, `${BackendPaths.votePost.getPath({ id: event.payload.id })}`, 'POST', params);
        const posts = yield select(currentPosts);
        const currentPostState = posts[postWithLikes.id];
        const updatedPost = {
            ...currentPostState,
            ...postWithLikes,
        };
        const normalizedData = normalize(updatedPost, post);
        yield put({ type: postConstants.VOTE_POST_SUCCESS, payload: { ...normalizedData } });
    } catch (e) {
        yield put({ type: postConstants.VOTE_POST_FAILURE, payload: e });
    }
}

function* watchVotePost() {
    yield takeEvery(postConstants.VOTE_POST_REQUEST, votePost);
}

function* fetchUserPosts(event) {
    try {
        const limit = 20;
        const offset = (event.payload && event.payload.offset) || 0;
        const userPosts = yield call(api, BackendPaths.userPosts.getPath({ limit, offset }), 'GET', {
            withCredentials: true,
        });
        const normalizedData = normalize(userPosts, postList);
        yield put({ type: postConstants.FETCH_USER_POSTS_SUCCESS, payload: { ...normalizedData, hasMorePosts: userPosts.hasMore } });
    } catch (e) {
        yield put({ type: postConstants.FETCH_USER_POSTS_FAILURE });
    }
}

function* watchUserPosts() {
    yield takeEvery(postConstants.FETCH_USER_POSTS_REQUEST, fetchUserPosts);
}


function* fetchHotPosts(event) {
    try {
        const limit = 20;
        const offset = (event.payload && event.payload.offset) || 0;
        const hotPosts = yield call(api, BackendPaths.hotPosts.getPath({ limit, offset }), 'GET');
        const normalizedData = normalize({ posts: hotPosts }, postList);
        yield put({ type: postConstants.FETCH_HOT_POSTS_SUCCESS, payload: { ...normalizedData, hasMorePosts: hotPosts.hasMore } });
    } catch (e) {
        yield put({ type: postConstants.FETCH_HOT_POSTS_FAILURE });
    }
}

function* watchHotPosts() {
    yield takeEvery(postConstants.FETCH_HOT_POSTS_REQUEST, fetchHotPosts);
}


function* fetchPostAndReplies(event) {
    try {
        const postWithReplies = yield call(api, `${BackendPaths.postAndReplies.getPath({ id: event.payload })}`, 'GET');
        const normalizedData = normalize(postWithReplies, post);
        yield put({ type: postConstants.FETCH_POST_AND_REPLIES_SUCCESS, payload: normalizedData });
    } catch (e) {
        yield put({ type: postConstants.FETCH_POST_AND_REPLIES_FAILURE });
    }
}

function* watchPostAndReplies() {
    yield takeEvery(postConstants.FETCH_POST_AND_REPLIES_REQUEST, fetchPostAndReplies);
}


export default function* postSagas() {
    yield all([
        watchPostAndReplies(),
        watchHotPosts(),
        watchVotePost(),
        watchUserPosts(),
        watchVoteReply(),
        watchPostReply(),
        watchCreatePost(),
    ]);
}
