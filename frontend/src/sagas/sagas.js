/* eslint-disable import/prefer-default-export */
import { all } from 'redux-saga/effects';
import userSagas from './userSagas';
import formSagas from './formSagas';
import postSagas from './postSagas';

export function* rootSagas() {
    yield all([userSagas(), formSagas(), postSagas()]);
}
