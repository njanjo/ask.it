import React from 'react';
import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import connectedAuthWrapper from 'redux-auth-wrapper/connectedAuthWrapper';
import routes from './applicationRoutes';
import Loading from '../components/Loading';
import SignInSignUp from '../components/header/SessionControl';

const locationHelper = locationHelperBuilder({});

const userIsAuthenticatedDefaults = {
    authenticatedSelector: state => state.user.loggedIn,
    authenticatingSelector: state => state.user.loggingIn,
    wrapperDisplayName: 'UserIsAuthenticated',
};

export const userIsAuthenticated = connectedAuthWrapper(userIsAuthenticatedDefaults);

export const userIsAuthenticatedRedir = connectedRouterRedirect({
    ...userIsAuthenticatedDefaults,
    AuthenticatingComponent: Loading,
    redirectPath: routes.signin.path,
});

const userIsNotAuthenticatedDefaults = {
    // Want to redirect the user when they are done loading and authenticated
    authenticatedSelector: state => !state.user.loggedIn,
    authenticatingSelector: state => state.user.loggingIn,
    AuthenticatingComponent: Loading,
    wrapperDisplayName: 'UserIsNotAuthenticated',
};

export const userIsNotAuthenticated = connectedAuthWrapper(userIsNotAuthenticatedDefaults);

export const userIsNotAuthenticatedRedir = connectedRouterRedirect({
    ...userIsNotAuthenticatedDefaults,
    redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || '/',
    allowRedirectBack: false,
});

export const visibleOnlyWhenLoggedIn = connectedAuthWrapper({
    authenticatedSelector: state => state.user.loggedIn,
    wrapperDisplayName: 'AvatarOrSignIn',
    FailureComponent: () => <SignInSignUp />,
});
