import Route from './route';

const backend = process.env.REACT_APP_BACKEND;
export default {
    whoami: new Route(`${backend}/user/whoami`),
    hotPosts: new Route(`${backend}/post/hot?limit=:limit&offset=:offset`),
    userPosts: new Route(`${backend}/post/all?limit=:limit&offset=:offset`),
    votePost: new Route(`${backend}/post/vote/:id`),
    voteReply: new Route(`${backend}/reply/vote/:id`),
    postReply: new Route(`${backend}/reply`),
    createPost: new Route(`${backend}/post`),
    session: new Route(`${backend}/session`),
    user: new Route(`${backend}/user`),
    postAndReplies: new Route(`${backend}/post/with-replies/:id`),
    checkEmail: new Route(`${backend}/user/check-email?email=:email`),
};
