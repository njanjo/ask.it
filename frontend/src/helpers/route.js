const LETTER_REGEX = /[a-zA-Z0-9-_]/;
const NOT_VARIABLE_REGEX = /[0-9/]/;

export class StringPart {
    constructor(part) {
        this.part = part;
    }

    parse(path, startFrom) {
        if (path.slice(startFrom).startsWith(this.part)) {
            return {
                valid: true,
                value: this.part,
                endIndex: startFrom + this.part.length,
            };
        }
        return {
            valid: false,
        };
    }

    getPath() {
        return this.part;
    }
}

export class ParamPart {
    constructor(part) {
        this.part = part;
        this.paramName = part.slice(1);
    }

    parse(path, startFrom) {
        const workingPart = path.slice(startFrom);
        const { length } = workingPart;
        let value = '';
        let i = 0;
        while (LETTER_REGEX.test(workingPart[i]) && i < length) {
            value += workingPart[i];
            i += 1;
        }
        return {
            value,
            valid: value.length > 0,
            key: this.paramName,
            endIndex: startFrom + i,
        };
    }

    getPath(params) {
        return params[this.paramName];
    }
}

export default class Route {
    constructor(path) {
        this.path = path;
        this.parts = [];
        this._parse();
    }

    isExactPath(path) {
        let pathIndex = 0;
        for (let i = 0; i < this.parts.length; i += 1) {
            const part = this.parts[i];
            const result = part.parse(path, pathIndex);
            if (!result.valid) {
                return false;
            }
            pathIndex = result.endIndex;
        }
        return pathIndex === path.length;
    }

    getParams(path) {
        const params = {};
        let pathIndex = 0;
        for (let i = 0; i < this.parts.length; i += 1) {
            const part = this.parts[i];
            const result = part.parse(path, pathIndex);
            if (!result.valid) {
                return undefined;
            }
            if (result.key) {
                params[result.key] = result.value;
            }
            pathIndex = result.endIndex;
        }
        return params;
    }

    getPath(params) {
        let path = '';
        for (let i = 0; i < this.parts.length; i += 1) {
            const part = this.parts[i];
            path += part.getPath(params);
        }
        return path;
    }

    _parse() {
        let lastPart = '';
        let isParam = false;
        for (let i = 0; i < this.path.length; i += 1) {
            const char = this.path[i];
            // if letter just append to lastPart
            if (LETTER_REGEX.test(char)) {
                lastPart += char;
                continue;
            }
            if (lastPart.length > 0) {
                const part = isParam
                    ? new ParamPart(lastPart)
                    : new StringPart(lastPart);
                this.parts.push(part);
            }
            lastPart = char;
            isParam = char === ':' && !NOT_VARIABLE_REGEX.test(this.path[i + 1]);
        }
        if (lastPart.length > 0) {
            const part = isParam ? new ParamPart(lastPart) : new StringPart(lastPart);
            this.parts.push(part);
        }
    }
}
