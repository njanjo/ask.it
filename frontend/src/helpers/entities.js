import { schema } from 'normalizr';

export const reply = new schema.Entity('replies', {
});


export const post = new schema.Entity('posts', {
    replies: [reply],
});

export const postList = { posts: [post] };
