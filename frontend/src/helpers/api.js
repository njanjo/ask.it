import { isStorageAvailable } from './util';

const STORAGE = window.localStorage;
const IS_STORAGE_AVAILABLE = isStorageAvailable(STORAGE);

export default function api(url, method, params) {
    const request = {
        headers: {},
        mode: 'cors', // no-cors, cors, *same-origin
    };
    request.method = method || 'GET';

    if (params && params.withCredentials) {
        if (IS_STORAGE_AVAILABLE) {
            const authorization = window.localStorage.getItem('bearer-token');
            if (authorization) {
                request.headers.Authorization = authorization;
            }
        }
    }

    if (params && params.body) {
        request.body = JSON.stringify(params.body);
        request.headers = {
            ...request.headers,
            'content-type': 'application/json',
        };
    }

    return new Promise((resolve, reject) => {
        fetch(url, request).then(response => {
            if (!response.ok) return reject(response.bodyUsed && response.json());
            const authorization = response.headers.get('authorization');
            if (params
        && params.storeCredentials
        && authorization
        && IS_STORAGE_AVAILABLE
            ) {
                window.localStorage.setItem('bearer-token', authorization);
            }
            return resolve(response.json());
        }, reject);
    });
}
