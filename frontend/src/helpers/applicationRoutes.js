import Route from './route';

export default {
    root: new Route('/'),
    myposts: new Route('/my-posts'),
    signup: new Route('/signup'),
    signin: new Route('/signin'),
    profile: new Route('/profile'),
    newpost: new Route('/new-post'),
    post: new Route('/post/:id'),
};
