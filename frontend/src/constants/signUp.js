export const SIGN_UP_REQUEST = '@@user/SIGN_UP_REQUEST';
export const SIGN_UP_SUCCESS = '@@user/SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = '@@user/SIGN_UP_FAILURE';
export const CHECK_EMAIL = '@@user/CHECK_EMAIL';
