import React from 'react';
import './loading.css';

const Loading = () => (
    <div className="spinner-wrapper">
        <div className="spinner">
            <div className="double-bounce1" />
            <div className="double-bounce2" />
        </div>
    </div>
);

export default Loading;
