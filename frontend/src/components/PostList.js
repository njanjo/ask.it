import React from 'react';
import PropTypes from 'prop-types';

import './post-list.css';
import Loading from './Loading';
import PostListItem from './PostListItem';

const PostList = ({
    fetchingPosts, posts, hasMorePosts, loadMore,
}) => {
    if (posts.length === 0) {
        return (
            <div className="container">
                <Loading />
            </div>
        );
    }

    let loadMoreSection;
    if (hasMorePosts && fetchingPosts) {
        loadMoreSection = (<Loading />);
    } else if (hasMorePosts) {
        loadMoreSection = (
            <button type="button" className="btn btn-primary load-more" onClick={loadMore}>Load More</button>
        );
    }

    return (
        <div className="container post-list">
            {posts.map(p => <PostListItem id={p.id} key={p.id} title={p.title} totalVotes={parseInt(p.totalVotes, 10)} />)}
            <div className="load-more-wrapper">
                {loadMoreSection}
            </div>
        </div>

    );
};

PostList.propTypes = {
    loadMore: PropTypes.func.isRequired,
    fetchingPosts: PropTypes.bool.isRequired,
    posts: PropTypes.array,
    hasMorePosts: PropTypes.bool,
};

PostList.defaultProps = {
    hasMorePosts: false,
    posts: [],
};

export default PostList;
