import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './user-info.css';
import Popup from 'reactjs-popup';
import Avatar from './avatar/Avatar';

const UserInfo = ({ user, signOut }) => (
    <Popup
        trigger={() => (
            <div className="user-info">
                <Avatar initials={`${user.firstName[0]}${user.lastName[0]}`} />
                <h5 className="text-light user-info__name">
                    {`${user.firstName} ${user.lastName}`}
                </h5>
            </div>
        )}
        position="bottom right"
        closeOnDocumentClick
    >
        <div className="user-info-popup">
            <Link className="nav-item" to="/profile">Profile</Link>
            <button type="button" className="btn-primary" onClick={signOut}>Sign Out</button>
        </div>
    </Popup>
);

UserInfo.propTypes = {
    user: PropTypes.object.isRequired,
    signOut: PropTypes.func.isRequired,
};

UserInfo.defaultProps = {};

export default UserInfo;
