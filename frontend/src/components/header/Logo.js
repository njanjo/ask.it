import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './logo.css';

const Logo = ({ small, light }) => (
    <div className={classNames('logo', { 'logo--small': small, 'logo--light': light })}>
        Ask.it
    </div>
);

Logo.propTypes = {
    small: PropTypes.bool,
    light: PropTypes.bool,
};

Logo.defaultProps = {
    small: false,
    light: false,
};

export default Logo;
