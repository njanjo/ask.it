import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './navigation.css';

const Navigation = ({ loggedIn }) => (
    <div className="navigation">
        <Link className="navigation__item" to="/">Hot</Link>
        {loggedIn && <Link className="navigation__item" to="/my-posts">Questions</Link>}
        {loggedIn && <Link className="navigation__item" to="/new-post">New Post</Link>}
    </div>
);

Navigation.propTypes = {
    loggedIn: PropTypes.bool,
};

Navigation.defaultProps = {
    loggedIn: false,
};

export default Navigation;
