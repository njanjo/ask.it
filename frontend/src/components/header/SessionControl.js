import React from 'react';
import { Link } from 'react-router-dom';

import './session-control.css';

const SessionControl = () => (
    <div className="session-control">
        <Link className="btn-primary" to="/signup">Sign Up</Link>
        <Link className="btn-primary" to="/signin">Sign In</Link>

    </div>
);

SessionControl.propTypes = {
};

SessionControl.defaultProps = {
};

export default SessionControl;
