import React from 'react';
import PropTypes from 'prop-types';
import Logo from './Logo';
import { visibleOnlyWhenLoggedIn } from '../../helpers/auth';
import Navigation from './Navigation';

import './header.css';
import UserInfo from '../UserInfo';

const LoggedInAvatar = visibleOnlyWhenLoggedIn(({ user, signOut }) => <UserInfo signOut={signOut} user={user} />);


const Header = ({ user, signOut, loggedIn }) => (
    <header>
        <nav>
            <div className="header__navigation-wrapper">
                <a href="/">
                    <Logo small light />
                </a>
                <Navigation loggedIn={loggedIn} />
            </div>
            <LoggedInAvatar signOut={signOut} user={user} />
        </nav>
    </header>
);


Header.propTypes = {
    signOut: PropTypes.func.isRequired,
    user: PropTypes.object,
    loggedIn: PropTypes.bool,
};

Header.defaultProps = {
    user: undefined,
    loggedIn: false,
};

export default Header;
