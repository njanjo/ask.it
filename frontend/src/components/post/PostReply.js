/* eslint-disable import/no-mutable-exports */
import React from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';

import './post-reply.css';
import TextAreaInput from '../input/TextAreaInput';

let PostReply = ({ postReply }) => (
    <div className="add-reply">
        <TextAreaInput name="reply" placeholder="Enter text here..." />
        <button className="btn-primary" type="button" onClick={postReply}>Post Reply</button>
    </div>
);

PostReply = reduxForm({
    form: 'postReply',
})(PostReply);

PostReply.propTypes = {
    postReply: PropTypes.func.isRequired,
};

PostReply.defaultProps = {
};

export default PostReply;
