import React from 'react';
import PropTypes from 'prop-types';

import './post-footer.css';
import Avatar from '../avatar/Avatar';

const PostFooter = ({ activePost }) => (
    <div className="post-footer">
        <div className="post-footer__user-area">
            <Avatar initials={`${activePost.firstName[0]}${activePost.lastName[0]}`} />
            <div>
                <span className="post-footer__user-name">{activePost.username}</span>
            </div>
        </div>
    </div>
);

PostFooter.propTypes = {
    activePost: PropTypes.object.isRequired,
};

PostFooter.defaultProps = {
};

export default PostFooter;
