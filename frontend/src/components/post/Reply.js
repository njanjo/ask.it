import React from 'react';
import PropTypes from 'prop-types';

import './reply.css';
import PostFooter from './PostFooter';
import PostVoter from './PostVoter';

const Reply = ({ reply, voteReply }) => (
    <div className="container-fluid reply">
        <div className="post-content">
            <PostVoter vote={voteReply} totalVotes={parseInt(reply.totalVotes, 10)} id={reply.id} />
            <p className="post-text">{reply.content}</p>
        </div>
        <PostFooter activePost={reply} />
    </div>
);

Reply.propTypes = {
    reply: PropTypes.object.isRequired,
    voteReply: PropTypes.func.isRequired,
};

Reply.defaultProps = {};

export default Reply;
