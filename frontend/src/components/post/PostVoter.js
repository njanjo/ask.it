import React from 'react';
import PropTypes from 'prop-types';

import './post-voter.css';
import VOTE from '../../constants/vote';

class PostVoter extends React.Component {
    upvote = () => {
        const { vote, id } = this.props;
        vote({ id, vote: VOTE.UPVOTE });
    }

    downvote = () => {
        const { vote, id } = this.props;
        vote({ id, vote: VOTE.DOWNVOTE });
    }

    render() {
        const {
            totalVotes,
        } = this.props;
        return (
            <div className="post-voter">
                <button type="button" className="vote-button btn-link" onClick={this.upvote}>
                    <i className="fas fa-caret-up" />
                </button>
                {totalVotes}
                <button type="button" className="vote-button btn-link" onClick={this.downvote}>
                    <i className="fas fa-caret-down" />
                </button>
            </div>
        );
    }
}
PostVoter.propTypes = {
    id: PropTypes.number.isRequired,
    totalVotes: PropTypes.number.isRequired,
    vote: PropTypes.func.isRequired,
};

PostVoter.defaultProps = {
};

export default PostVoter;
