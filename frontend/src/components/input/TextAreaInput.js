import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

const renderField = ({
    input, label, type, meta: { touched, error, warning },
}) => (
    <div>
        <textarea {...input} placeholder={label} type={type} className="form-control" />
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
);

const TextAreaInput = ({
    placeholder, name,
}) => (
    <div className="form-group">
        <Field
            component={renderField}
            name={name}
            className="form-control"
            label={placeholder}
        />
    </div>
);


TextAreaInput.propTypes = {
    placeholder: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,

};
TextAreaInput.defaultProps = {
};

export default TextAreaInput;
