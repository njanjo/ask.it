import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

const renderField = ({
    input, label, type, meta: { touched, error, warning },
}) => (
    <div>
        <input {...input} placeholder={label} type={type} className="form-control" />
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
);

const TextField = ({
    placeholder, type, name, required, onBlur, onChange, validate,
}) => (
    <div className="form-group">
        <Field
            component={renderField}
            validate={validate}
            onBlur={onBlur}
            onChange={onChange}
            required={required}
            name={name}
            type={type}
            className="form-control"
            label={placeholder}
        />
    </div>
);


TextField.propTypes = {
    required: PropTypes.bool,
    placeholder: PropTypes.string.isRequired,
    onBlur: PropTypes.func,
    validate: PropTypes.func,
    onChange: PropTypes.func,
    name: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['email', 'password', 'text']),

};
TextField.defaultProps = {
    type: 'text',
    required: false,
    onBlur: undefined,
    onChange: undefined,
    validate: undefined,
};

export default TextField;
