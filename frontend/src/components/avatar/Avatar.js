import React from 'react';
import PropTypes from 'prop-types';
import './avatar.css';


function getRandomColor(initials) {
    Math.seedrandom(initials);
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i += 1) {
        color = `${color}${letters[Math.floor(Math.random() * 16)]}`;
    }
    return color;
}

const Avatar = ({ initials }) => (
    <div className="avatar" style={{ backgroundColor: getRandomColor(initials) }}>
        {initials}
    </div>
);

Avatar.propTypes = {
    initials: PropTypes.string.isRequired,
};

Avatar.defaultProps = {
};

export default Avatar;
