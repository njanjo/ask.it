/* eslint-disable import/no-mutable-exports */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { reduxForm } from 'redux-form';
import TextField from './input/TextField';
import * as signUpConstants from '../constants/signUp';

let SignUpForm = ({
    formValid,
    signUp,
}) => (
    <form onSubmit={signUp}>
        <TextField
            required
            name="email"
            type="email"
            placeholder="Email"
        />
        <TextField
            name="firstName"
            placeholder="First Name"
        />
        <TextField
            name="lastName"
            placeholder="Last Name"
        />
        <TextField
            required
            name="password"
            type="password"
            placeholder="Password"
        />
        <TextField
            required
            name="confirmPassword"
            type="password"
            placeholder="Confirm Password"
        />
        <button type="button" onClick={signUp} className={classNames('btn', 'btn-primary', { disabled: !formValid })}>Sign Up</button>
    </form>);


const asyncValidate = (formData, dispatch) => new Promise((resolve, reject) => {
    dispatch({ type: signUpConstants.CHECK_EMAIL, payload: { formData, resolve, reject } });
});
const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    } else if (!values.password) {
        errors.password = 'Required';
    } else if (!values.confirmPassword) {
        errors.confirmPassword = 'Required';
    } else if (values.confirmPassword && values.password && values.confirmPassword !== values.password) {
        errors.password = 'Passwords must match';
        errors.confirmPassword = 'Passwords must match';
    }
    return errors;
};


SignUpForm = reduxForm({
    form: 'signUp',
    validate,
    asyncValidate,
})(SignUpForm);

SignUpForm.propTypes = {
    formValid: PropTypes.bool,
    signUp: PropTypes.func.isRequired,
};

SignUpForm.defaultProps = {
    formValid: false,
};


export default SignUpForm;
