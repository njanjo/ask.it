import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './post-list-item.css';

const PostListItem = ({ title, totalVotes, id }) => (
    <div className="post-list-item">
        <div className="post-list-item__votes">
            {totalVotes}
            <h6>votes</h6>
        </div>
        <Link className="card-link" to={`/post/${id}`}>
            <h4>{title}</h4>
        </Link>


    </div>
);

PostListItem.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    totalVotes: PropTypes.number.isRequired,
};

PostListItem.defaultProps = {
};

export default PostListItem;
