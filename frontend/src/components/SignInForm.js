/* eslint-disable import/no-mutable-exports */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { reduxForm } from 'redux-form';
import TextField from './input/TextField';

let SignInForm = ({
    formValid,
    signIn,
}) => (
    <form onSubmit={signIn}>
        <TextField
            required
            name="email"
            type="email"
            placeholder="Email"
        />
        <TextField
            required
            name="password"
            type="password"
            placeholder="Password"
        />
        <button type="button" onClick={signIn} className={classNames('btn', 'btn-primary', { disabled: !formValid })}>Sign In</button>
    </form>);


const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    } else if (!values.password) {
        errors.password = 'Required';
    }
    return errors;
};


SignInForm = reduxForm({
    form: 'signIn',
    validate,
})(SignInForm);

SignInForm.propTypes = {
    signIn: PropTypes.func.isRequired,
    formValid: PropTypes.bool,
};

SignInForm.defaultProps = {
    formValid: false,
};


export default SignInForm;
