/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import { Route } from 'react-router-dom';
import routes from '../helpers/applicationRoutes';
import * as signInConstants from '../constants/signIn';

import {
    userIsAuthenticatedRedir,
    userIsNotAuthenticatedRedir,
} from '../helpers/auth';

import Hot from './pages/hot/Hot';
import MyPosts from './pages/myPosts/MyPosts';
import NewPost from './pages/newPost/NewPost';
import Post from './pages/post/Post';
import SignIn from './pages/signIn/SignIn';
import SignUp from './pages/signUp/SignUp';
import Profile from './pages/Profile';
import Header from '../components/header/Header';

const PostPage = Post;
const HotPage = Hot;
const SignInPage = userIsNotAuthenticatedRedir(SignIn);
const SignUpPage = userIsNotAuthenticatedRedir(SignUp);
const ProfilePage = userIsAuthenticatedRedir(Profile);
const NewPostPage = userIsAuthenticatedRedir(NewPost);
const MyPostsPage = userIsAuthenticatedRedir(MyPosts);

class RootContainer extends React.Component {
    componentWillMount() {
        const { fetchMe } = this.props;
        fetchMe();
    }

    render() {
        const { user, signOut, loggedIn } = this.props;
        return (
            <ConnectedRouter history={this.props.history}>
                <React.Fragment>
                    <Header loggedIn={loggedIn} signOut={signOut} user={user} />
                    <Route exact path={routes.root.path} component={HotPage} />
                    <Route exact path={routes.myposts.path} component={MyPostsPage} />
                    <Route exact path={routes.post.path} component={PostPage} />
                    <Route path={routes.signin.path} component={SignInPage} />
                    <Route path={routes.signup.path} component={SignUpPage} />
                    <Route path={routes.profile.path} component={ProfilePage} />
                    <Route path={routes.newpost.path} component={NewPostPage} />
                </React.Fragment>
            </ConnectedRouter>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.current,
    loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
    fetchMe: () => dispatch({
        type: signInConstants.FETCH_ME_REQUEST,
    }),
    signOut: () => dispatch({
        type: signInConstants.SIGN_OUT_REQUEST,
    }),
});

RootContainer.propTypes = {
    loggedIn: PropTypes.bool,
    user: PropTypes.object,
    history: PropTypes.object.isRequired,
    fetchMe: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired,
};
RootContainer.defaultProps = {
    user: undefined,
    loggedIn: false,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RootContainer);
