import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as signUpConstants from '../constants/signUp';
import SignUpForm from '../components/SignUpForm';
import { isSignUpFormValidSelector } from '../sagas/selectors';

const SignUpFormWrapper = ({
    signUp,
    formValid,
}) => (
    <React.Fragment>
        <SignUpForm
            formValid={formValid}
            signUp={signUp}
        />
    </React.Fragment>
);
const mapStateToProps = state => ({
    formValid: isSignUpFormValidSelector(state),
});

const mapDispatchToProps = dispatch => ({
    signUp: payload => dispatch({
        type: signUpConstants.SIGN_UP_REQUEST,
        payload,
    }),
    checkEmail: payload => dispatch({
        type: signUpConstants.CHECK_EMAIL,
        payload,
    }),
});

SignUpFormWrapper.propTypes = {
    formValid: PropTypes.bool,
    signUp: PropTypes.func.isRequired,
};

SignUpFormWrapper.defaultProps = {
    formValid: false,
};


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SignUpFormWrapper);
