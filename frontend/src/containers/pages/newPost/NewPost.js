/* eslint-disable import/no-mutable-exports */
import React from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import * as postConstants from '../../../constants/post';
import TextAreaInput from '../../../components/input/TextAreaInput';
import TextField from '../../../components/input/TextField';

import './new-post.css';
import { isCreatePostFormValidSelector } from '../../../sagas/selectors';

let NewPost = ({ newPost, formValid }) => (
    <form className="container new-post">
        <TextField
            required
            name="title"
            type="text"
            placeholder="Title"
        />
        <TextAreaInput required name="content" placeholder="Enter text here..." />
        <button className={classNames('btn', 'btn-primary', { disabled: !formValid })} type="button" onClick={newPost}>Create Post</button>
    </form>
);

const validate = values => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Required';
    } else if (!values.content) {
        errors.postContent = 'Required';
    }
    return errors;
};

const mapStateToProps = state => ({
    formValid: isCreatePostFormValidSelector(state),
});

const mapDispatchToProps = dispatch => ({
    newPost: payload => dispatch({
        type: postConstants.CREATE_NEW_POST_REQUEST,
        payload,
    }),
});

NewPost = reduxForm({
    form: 'createPost',
    validate,
})(NewPost);

NewPost.propTypes = {
    formValid: PropTypes.bool,
    newPost: PropTypes.func.isRequired,
};

NewPost.defaultProps = {
    formValid: false,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(NewPost);
