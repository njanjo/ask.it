import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as postConstants from '../../../constants/post';

import './my-posts.css';
import { userPostsAsArraySelector } from '../../../sagas/selectors';
import PostList from '../../../components/PostList';

class MyPosts extends React.Component {
    componentWillMount() {
        const { fetchUserPosts } = this.props;
        fetchUserPosts();
    }

    loadMore = () => {
        const { fetchUserPosts, posts } = this.props;
        fetchUserPosts({ offset: posts.length });
    }

    render() {
        const { posts, fetchingPosts, hasMorePosts } = this.props;
        return (<PostList loadMore={this.loadMore} posts={posts} fetchingPosts={fetchingPosts} hasMorePosts={hasMorePosts} />);
    }
}

const mapStateToProps = state => ({
    posts: userPostsAsArraySelector(state),
    fetchingPosts: state.post.fetchingPosts,
    hasMorePosts: state.post.hasMorePosts,
});

const mapDispatchToProps = dispatch => ({
    fetchUserPosts: payload => dispatch({
        type: postConstants.FETCH_USER_POSTS_REQUEST,
        payload,
    }),
});

MyPosts.propTypes = {
    fetchUserPosts: PropTypes.func.isRequired,
    posts: PropTypes.array,
    fetchingPosts: PropTypes.bool,
    hasMorePosts: PropTypes.bool,
};

MyPosts.defaultProps = {
    posts: [],
    fetchingPosts: false,
    hasMorePosts: false,
};


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MyPosts);
