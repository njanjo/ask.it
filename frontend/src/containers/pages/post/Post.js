import React from 'react';
import PropTypes from 'prop-types';
import { reset } from 'redux-form';
import { connect } from 'react-redux';
import * as postConstants from '../../../constants/post';
import Loading from '../../../components/Loading';
import PostFooter from '../../../components/post/PostFooter';

import './post.css';
import PostVoter from '../../../components/post/PostVoter';
import Reply from '../../../components/post/Reply';
import PostReply from '../../../components/post/PostReply';
import { visibleOnlyWhenLoggedIn } from '../../../helpers/auth';
import { postRepliesArraySelector } from '../../../sagas/selectors';

const LoggedInPostReply = visibleOnlyWhenLoggedIn(({ postReply }) => <PostReply postReply={postReply} />);


class Post extends React.Component {
    componentWillMount() {
        const { fetchPostAndReplies, match } = this.props;
        fetchPostAndReplies(match.params.id);
    }

    componentWillReceiveProps(nextProps) {
        const { match } = this.props;
        if (nextProps.match && nextProps.match.params.id === match.params.id) return;
        const { fetchPostAndReplies } = nextProps;
        fetchPostAndReplies(nextProps.match.params.id);
    }

    postReply = e => {
        const { postReply, match, resetPostReplyForm } = this.props;
        postReply({ postId: parseInt(match.params.id, 10) });
        resetPostReplyForm();
    }

    render() {
        const {
            fetchingPostAndReplies, posts, match, votePost, replies, voteReply,
        } = this.props;
        if (fetchingPostAndReplies || !posts[match.params.id] || posts[match.params.id].replies === undefined) {
            return (
                <div className="container">
                    <Loading />
                </div>
            );
        }
        const activePost = posts[match.params.id];
        return (
            <div className="post-page">
                <div className="post">
                    <h2 className="post-title">{activePost.title}</h2>
                    <div className="post-content">
                        <PostVoter vote={votePost} totalVotes={parseInt(activePost.totalVotes, 10)} id={activePost.id} />
                        <p className="post-text">{activePost.content}</p>
                    </div>
                    <PostFooter activePost={activePost} />
                </div>
                <div className="">
                    {replies.filter(r => r.postId === activePost.id).sort((a, b) => b.totalVotes - a.totalVotes).map(r => <Reply voteReply={voteReply} key={r.id} reply={r} />)}
                </div>
                <LoggedInPostReply postReply={this.postReply} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    fetchingPostAndReplies: state.post.fetchingPostAndReplies,
    posts: state.post.posts,
    replies: postRepliesArraySelector(state),
    users: state.post.users,
});

const mapDispatchToProps = dispatch => ({
    resetPostReplyForm: () => dispatch(reset('postReply')),
    postReply: payload => dispatch({
        type: postConstants.POST_REPLY_REQUEST,
        payload,
    }),
    votePost: payload => dispatch({
        type: postConstants.VOTE_POST_REQUEST,
        payload,
    }),
    voteReply: payload => dispatch({
        type: postConstants.VOTE_REPLY_REQUEST,
        payload,
    }),
    fetchPostAndReplies: payload => dispatch({
        type: postConstants.FETCH_POST_AND_REPLIES_REQUEST,
        payload,
    }),
});

Post.propTypes = {
    fetchingPostAndReplies: PropTypes.bool,
    fetchPostAndReplies: PropTypes.func.isRequired,
    resetPostReplyForm: PropTypes.func.isRequired,
    postReply: PropTypes.func.isRequired,
    votePost: PropTypes.func.isRequired,
    voteReply: PropTypes.func.isRequired,
    posts: PropTypes.object,
    replies: PropTypes.array ,
    match: PropTypes.shape({
        params: PropTypes.shape({
            id: PropTypes.string,
        }),
    }).isRequired,
};

Post.defaultProps = {
    posts: {},
    replies: {},
    fetchingPostAndReplies: false,
};


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Post);
