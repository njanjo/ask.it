import React from 'react';
import Logo from '../../../components/header/Logo';
import '../signUp/sign-up.css';
import SignInFormWrapper from '../../SignInFormWrapper';

const SignIn = () => (
    <div className="container sign-up">
        <Logo />
        <SignInFormWrapper />
    </div>
);

SignIn.propTypes = {
};

SignIn.defaultProps = {};

export default SignIn;
