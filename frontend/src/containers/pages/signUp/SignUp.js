import React from 'react';
import Logo from '../../../components/header/Logo';
import './sign-up.css';
import SignUpFormWrapper from '../../SignUpFormWrapper';

const SignUp = () => (
    <div className="container sign-up">
        <Logo />
        <SignUpFormWrapper />
    </div>
);

SignUp.propTypes = {
};

SignUp.defaultProps = {};

export default SignUp;
