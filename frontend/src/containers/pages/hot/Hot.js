import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as postConstants from '../../../constants/post';
import './hot.css';
import { hotPostsAsArraySelector } from '../../../sagas/selectors';
import PostList from '../../../components/PostList';

class Hot extends React.Component {
    componentWillMount() {
        const { fetchHotPosts } = this.props;
        fetchHotPosts();
    }

    loadMore = () => {
        const { fetchHotPosts, posts } = this.props;
        fetchHotPosts({ offset: posts.length });
    }

    render() {
        const { posts, fetchingPosts, hasMorePosts } = this.props;
        return (<PostList loadMore={this.loadMore} posts={posts} fetchingPosts={fetchingPosts} hasMorePosts={hasMorePosts} />);
    }
}

const mapStateToProps = state => ({
    hasMorePosts: state.post.hasMorePosts,
    posts: hotPostsAsArraySelector(state),
    fetchingPosts: state.post.fetchingPosts,
});

const mapDispatchToProps = dispatch => ({
    fetchHotPosts: payload => dispatch({
        type: postConstants.FETCH_HOT_POSTS_REQUEST,
        payload,
    }),
});

Hot.propTypes = {
    fetchHotPosts: PropTypes.func.isRequired,
    posts: PropTypes.array,
    fetchingPosts: PropTypes.bool,
    hasMorePosts: PropTypes.bool,
};

Hot.defaultProps = {
    posts: [],
    fetchingPosts: false,
    hasMorePosts: false,
};


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Hot);
