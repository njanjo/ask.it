import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as signInConstants from '../constants/signIn';
import SignInForm from '../components/SignInForm';
import { isSignInFormValidSelector } from '../sagas/selectors';

const SignInFormWrapper = ({
    signIn,
    formValid,
}) => (
    <React.Fragment>
        <SignInForm
            formValid={formValid}
            signIn={signIn}
        />
    </React.Fragment>
);
const mapStateToProps = state => ({
    formValid: isSignInFormValidSelector(state),
});

const mapDispatchToProps = dispatch => ({
    signIn: payload => dispatch({
        type: signInConstants.SIGN_IN_REQUEST,
        payload,
    }),
});

SignInFormWrapper.propTypes = {
    formValid: PropTypes.bool,
    signIn: PropTypes.func.isRequired,
};

SignInFormWrapper.defaultProps = {
    formValid: false,
};


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SignInFormWrapper);
