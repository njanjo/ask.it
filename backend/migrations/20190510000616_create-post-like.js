exports.up = knex => knex.schema.createTable('post_likes', t => {
    t.primary(['userId', 'postId']);
    t.integer('userId', 255)
        .notNullable()
        .references('id')
        .inTable('users');
    t.integer('postId', 255)
        .notNullable()
        .references('id')
        .inTable('posts');
    t.integer('vote', 1).defaultTo(0);
});

exports.down = knex => knex.schema.dropTable('post_likes');
