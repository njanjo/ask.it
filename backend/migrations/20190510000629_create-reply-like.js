exports.up = knex => knex.schema.createTable('reply_likes', t => {
    t.primary(['userId', 'replyId']);
    t.integer('userId', 255)
        .notNullable()
        .references('id')
        .inTable('users');
    t.integer('replyId', 255)
        .notNullable()
        .references('id')
        .inTable('replies');
    t.integer('vote', 1).defaultTo(0);
});

exports.down = knex => knex.schema.dropTable('reply_likes');
