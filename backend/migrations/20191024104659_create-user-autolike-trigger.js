exports.up = async knex => {
    await knex.raw('\n'
        + 'CREATE OR REPLACE FUNCTION user_post_self_vote()\n'
        + '    RETURNS TRIGGER\n'
        + 'AS $$\n'
        + 'BEGIN\n'
        + '    INSERT INTO post_likes ("userId", "postId", "vote") VALUES (NEW."userId", NEW.id, 1);\n'
        + '    RETURN NEW;\n'
        + 'END;\n'
        + '$$\n'
        + '    LANGUAGE plpgsql;');
    await knex.raw('\n'
        + 'CREATE TRIGGER user_post_created\n'
        + '    AFTER INSERT\n'
        + '    ON posts\n'
        + '    FOR EACH ROW\n'
        + 'EXECUTE PROCEDURE user_post_self_vote();\n');
};


exports.down = async knex => {
    await knex.raw('DROP TRIGGER IF EXISTS user_post_created ON posts;');
    await knex.raw('DROP FUNCTION IF EXISTS user_post_self_vote;');
};
