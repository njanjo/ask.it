exports.up = knex => knex.schema.createTable('replies', t => {
    t.increments('id').unsigned().primary();
    t.integer('userId', 255)
        .notNullable()
        .references('id')
        .inTable('users');
    t.integer('postId', 255)
        .notNullable()
        .references('id')
        .inTable('posts');
    t.text('content').notNullable();
    t.boolean('chosen').defaultTo(false);
    t.dateTime('createdAt').notNullable().defaultTo(knex.fn.now());
    t.dateTime('updatedAt').nullable();
});

exports.down = knex => knex.schema.dropTable('replies');
