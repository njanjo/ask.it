exports.up = knex => knex.schema.createTable('posts', t => {
    t.increments('id').unsigned().primary();
    t.integer('userId', 255)
        .notNullable()
        .references('id')
        .inTable('users');
    t.string('title', 250).notNullable();
    t.text('content').notNullable();
    t.dateTime('createdAt').notNullable().defaultTo(knex.fn.now());
    t.dateTime('updatedAt').nullable();
});

exports.down = knex => knex.schema.dropTable('posts');
