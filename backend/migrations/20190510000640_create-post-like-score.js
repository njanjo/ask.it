exports.up = knex => knex.raw('CREATE OR REPLACE VIEW "post_like_scores" AS (SELECT "postId" AS "postId", SUM(vote) AS "totalVotes" FROM "post_likes" GROUP BY "postId")');


exports.down = knex => knex.raw('DROP VIEW "post_like_scores"');
