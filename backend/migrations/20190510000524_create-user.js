exports.up = knex => knex.schema.createTable('users', t => {
    t.increments('id').unsigned().primary();
    t.string('firstName', 255);
    t.string('lastName', 255);
    t.string('username', 255).unique().notNullable();
    t.string('email', 255).unique().notNullable();
    t.string('passhash', 255).notNullable();
    t.string('salt', 255).notNullable();
    t.dateTime('createdAt').notNullable().defaultTo(knex.fn.now());
    t.dateTime('updatedAt').nullable();
    t.index(['username']);
    t.index(['email']);
});

exports.down = knex => {
    knex.schema.table('users', t => {
        t.dropIndex(['username']);
    });
    knex.schema.table('users', t => {
        t.dropIndex(['email']);
    });
    return knex.schema.dropTable('users');
};
