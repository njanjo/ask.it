/* eslint-disable import/prefer-default-export */
const environment = process.env.NODE_ENV || 'development';


const configFile = require('./config.json');
const knexfile = require('./knexfile');


const config = configFile[environment];

if (config === undefined) {
    throw Error('No config found with that env variable');
}

module.exports = {
    postgresConfig: knexfile[environment],
    jwtConfig: config.jwt,
};
