// Update with your config settings.

module.exports = {

    development: {
        client: 'pg',
        connection: {
            host: 'localhost',
            database: 'askit_development',
            user: 'askit',
            password: 'Askit128',
        },
        pool: {
            min: 2,
            max: 10,
        },
        seeds: {
            directory: '../seeds',
        },
        migrations: {
            directory: '../migrations',
            tableName: 'knex_migrations',
        },
    },

    production: {
        client: 'pg',
        connection: {
            host: 'localhost',
            database: 'askit_production',
            user: 'askit',
            password: 'Askit128',
        },
        pool: {
            min: 2,
            max: 10,
        },
        seeds: {
            directory: '../seeds',
        },
        migrations: {
            directory: '../migrations',
            tableName: 'knex_migrations',
        },
    },
    test: {
        client: 'pg',
        connection: {
            host: 'localhost',
            database: 'askit_test',
            user: 'askit',
            password: 'Askit128',
        },
        pool: {
            min: 2,
            max: 10,
        },
        seeds: {
            directory: '../seeds',
        },
        migrations: {
            directory: '../migrations',
            tableName: 'knex_migrations',
        },
        log: {
            warn(message) {
                console.log(message);
            },
            error(message) {
                console.log(message);
            },
            deprecate(message) {
                console.log(message);
            },
            debug(message) {
                console.log(message);
            },
        },

    },

};
