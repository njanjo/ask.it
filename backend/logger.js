const bunyan = require('bunyan');

exports.loggerInstance = bunyan.createLogger({
    name: 'transaction-notifier',
    serializers: {
        req: require('bunyan-express-serializer'),
        res: bunyan.stdSerializers.res,
        err: bunyan.stdSerializers.err,
    },
    level: 'info',
    streams: [
        {
            level: 'debug',
            stream: process.stdout, // log INFO and above to stdout
        },
        {
            level: 'info',
            path: './logs/askit-backend.json', // log ERROR and above to a file
        },
    ],
});


exports.logResponse = function (id, body, statusCode) {
    const log = this.loggerInstance.child({
        id,
        body,
        statusCode,
    }, true);
    log.info('response');
};
