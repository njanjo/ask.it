const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const errorHandler = require('errorhandler');
const user = require('./src/routes/user');
const session = require('./src/routes/session');
const post = require('./src/routes/post');
const reply = require('./src/routes/reply');
const logger = require('./logger');


const app = express();
const port = 4000;

if (process.env.NODE_ENV !== 'production') {
    app.use(errorHandler());
}

app.use((req, res, next) => {
    const log = logger.loggerInstance.child({
        id: req.id,
        body: req.body,
    }, true);
    log.info({ req });
    next();
});

app.use(cors({
    allowedHeaders: ['Authorization', 'Content-Type'],
    exposedHeaders: ['Authorization'],
}));
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello from the API side'));

app.use('/user', user);
app.use('/session', session);
app.use('/post', post);
app.use('/reply', reply);

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        errors: {
            message: err.message,
            error: {},
        },
    });
});

app.listen(port, () => console.log(`Backend API listening on port ${port}!`));
