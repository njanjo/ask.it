const faker = require('faker');

const createFakeReply = (knex, post, user) => ({
    userId: user.id,
    postId: post.id,
    content: faker.lorem.text(),
    createdAt: knex.fn.now(),
    updatedAt: knex.fn.now(),
})

exports.seed = async knex => {
    const fakeReply = [];

    const users = await knex.raw(
        'SELECT id from "users" ORDER BY RANDOM() LIMIT 10;'
    );

    const posts = await knex.raw(
        'SELECT id from "posts";'
    );
    posts.rows.forEach(post => {
        users.rows.forEach(user => {
            fakeReply.push(createFakeReply(knex, post, user));
        });
    });

    await knex('replies').del().insert(fakeReply);
};
