const faker = require('faker');

const createFakePost = (knex, userId) => ({
    userId,
    title: faker.lorem.sentence(),
    content: faker.lorem.text(),
    createdAt: knex.fn.now(),
    updatedAt: knex.fn.now(),
});

exports.seed = async knex => {
    const fakePosts = [];

    const users = await knex.raw(
        'SELECT id from "users" ORDER BY RANDOM() LIMIT 50;'
    );


    users.rows.forEach(user => {
        for (let i = 0; i < 5; i += 1) {
            fakePosts.push(createFakePost(knex, user.id));
        }
    });

    await knex('posts').del().insert(fakePosts);
};
