const createFakePostLike = (post, user) => ({
    userId: user.id,
    postId: post.id,
    vote: Math.floor(Math.random() * Math.floor(3)) - 1,
})

exports.seed = async knex => {
    const fakePostLike = [];

    const users = await knex.raw(
        'SELECT id from "users" ORDER BY RANDOM() LIMIT 30;'
    );

    const posts = await knex.raw(
        'SELECT id from "posts";'
    );
    posts.rows.forEach(post => {
        users.rows.forEach(user => {
            fakePostLike.push(createFakePostLike(post, user));
        });
    });

    await knex('post_likes').del().insert(fakePostLike);
};
