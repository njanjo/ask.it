const faker = require('faker');

const createFakeUser = knex => ({
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    username: faker.internet.userName(),
    passhash: 'abcaaf125c121011d23693e67b712faf610fd8c2043d82e342ad9da71a3fa0455a6ff75facd90ab50e4f66ce113054f6d89dcb50a5b26a591c0320b76957c863',
    salt: '3de702ca0c569512',
    createdAt: knex.fn.now(),
    updatedAt: knex.fn.now(),
});

exports.seed = async knex => {
    const fakeUsers = [];

    for (let i = 0; i < 100; i += 1) {
        fakeUsers.push(createFakeUser(knex));
    }

    await knex('users').del().insert(fakeUsers);
};
