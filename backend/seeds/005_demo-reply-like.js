const createFakePostReplyLike = (reply, user) => ({
    userId: user.id,
    replyId: reply.id,
    vote: Math.floor(Math.random() * Math.floor(3)) - 1,
})

exports.seed = async knex => {
    const fakeReplyLike = [];

    const users = await knex.raw(
        'SELECT id from "users" ORDER BY RANDOM() LIMIT 5;'
    );

    const replies = await knex.raw(
        'SELECT id from "replies";'
    );
    replies.rows.forEach(reply => {
        users.rows.forEach(user => {
            fakeReplyLike.push(createFakePostReplyLike(reply, user));
        });
    });

    await knex('reply_likes').del().insert(fakeReplyLike);
};
