/* eslint-disable eqeqeq */
const HttpStatus = require('http-status-codes');
const ReplyAction = require('../actions/reply');
const PostAction = require('../actions/post');
const UserAction = require('../actions/user');


async function createReply(req, res) {
    const { userId } = req.token;
    const { postId, content } = req.body;
    const replyBody = {
        userId,
        postId,
        content,
    };
    try {
        const post = await PostAction.findById(postId);
        if (post == undefined) {
            return res.status(HttpStatus.NOT_FOUND).json({
                error: 'Cannot post reply to that post',
            });
        }
        const reply = await ReplyAction.create(replyBody);
        return res.status(HttpStatus.CREATED).json(reply);
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}

async function retrieveReply(req, res) {
    const { replyId } = req.body;
    try {
        const reply = await ReplyAction.findById(replyId);
        if (reply == undefined) return res.status(HttpStatus.NO_CONTENT).end();
        return res.status(HttpStatus.OK).json(reply);
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}

async function updateReply(req, res) {
}

async function deleteReply(req, res) {
}

async function voteReply(req, res) {
    const { id } = req.params;
    const { vote } = req.body;
    const userId = req.token.id;
    try {
        const existingLike = await ReplyAction.findReplyLike({ replyId: id, userId });
        if (existingLike != undefined) {
            if (existingLike.vote === vote) {
                return res.status(HttpStatus.CONFLICT).json({ error: 'Already voted' }).end();
            }
            await existingLike.destroy();
        } else {
            await ReplyAction.voteReply({ replyId: id, userId, vote });
        }
        const reply = await ReplyAction.findReplyWithLikes(id);
        return res.status(HttpStatus.OK).json(reply[0].output);
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}


async function allReplies(req, res) {
    const userId = req.query.userId || req.token.id;
    if (userId == undefined) return res.status(HttpStatus.BAD_REQUEST).end();
    try {
        const posts = await ReplyAction.findByUserId(userId);
        return res.status(HttpStatus.OK).json(posts);
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}

async function fromPost(req, res) {
    const { id } = req.params;
    const { limit, offset } = req.query;
    try {
        const { results, hasMore } = await ReplyAction.findByPostId({ id, limit, offset });
        return res.status(HttpStatus.OK).json({ ...results.map(r => r.output), hasMore });
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}


module.exports = {
    createReply,
    deleteReply,
    updateReply,
    retrieveReply,
    allReplies,
    voteReply,
    fromPost,
};
