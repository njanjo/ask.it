/* eslint-disable eqeqeq */
const HttpStatus = require('http-status-codes');
const PostAction = require('../actions/post');
const ReplyAction = require('../actions/reply');


async function createPost(req, res) {
    const { userId } = req.token;
    const postBody = { ...req.body };
    postBody.userId = userId;
    try {
        const post = await PostAction.create(postBody);
        return res.status(HttpStatus.CREATED).json(post);
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}

async function retrievePost(req, res) {
    const { id } = req.query;
    try {
        const post = await PostAction.findById(id);
        if (post == undefined) return res.status(HttpStatus.NO_CONTENT).end();
        return res.status(HttpStatus.OK).json(post);
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}

async function retrievePostAndReplies(req, res) {
    const { id } = req.params;
    try {
        const post = await PostAction.findPostWithLikes(id);
        if (post == undefined) return res.status(HttpStatus.NO_CONTENT).end();
        const { results, hasMore } = await ReplyAction.findRepliesWithLikes({ postId: id });
        return res.status(HttpStatus.OK).json({
            ...post,
            replies: results,
            hasMore,
        });
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}

async function votePost(req, res) {
    const { id } = req.params;
    const { vote } = req.body;
    const { userId } = req.token;
    try {
        const existingLike = await PostAction.findPostLike({ postId: id, userId });
        if (existingLike != undefined) {
            if (existingLike.vote === vote) {
                return res.status(HttpStatus.CONFLICT).json({ error: 'Already voted' }).end();
            }
            await PostAction.deletePostLike({ postId: id, userId });
        } else {
            await PostAction.votePost({ postId: id, userId, vote });
        }
        const post = await PostAction.findPostWithLikes(id);
        return res.status(HttpStatus.OK).json(post);
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}


async function deletePost(req, res) {
}

async function allPosts(req, res) {
    const userId = req.query.userId || req.token.userId;
    const { limit, offset } = req.query;
    if (userId == undefined) return res.status(HttpStatus.BAD_REQUEST).end();
    try {
        const { results, hasMore } = await PostAction.findByUserId({ userId, limit, offset });
        return res.status(HttpStatus.OK).json({ posts: results, hasMore });
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}

async function hot(req, res) {
    const { limit, offset } = req.query;
    try {
        const { results, hasMore } = await PostAction.findMostLiked({ limit, offset });
        return res.status(HttpStatus.OK).json({ ...results, hasMore });
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e.toString()).end();
    }
}


module.exports = {
    createPost,
    deletePost,
    votePost,
    retrievePost,
    retrievePostAndReplies,
    allPosts,
    hot,
};
