/* eslint-disable eqeqeq */
const HttpStatus = require('http-status-codes');
const UserAction = require('../actions/user');
const SessionAction = require('../actions/session');

async function create(req, res) {
    const existingUser = await UserAction.findByEmail(req.body.email);
    if (existingUser != undefined) {
        return res.status(HttpStatus.CONFLICT).json({
            error: 'User already exists with those credentials',
        });
    }
    const params = {
        ...req.body,
        username: await UserAction.generateUsername(req.body.email),
    };

    try {
        const user = await UserAction.create(params);
        const session = SessionAction.create(user.id);
        return res.status(HttpStatus.CREATED).set('Authorization', `Token ${session.token}`).json(user[0]);
    } catch (e) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e).end();
    }
}

async function retrieve(req, res) {
    const { id, username, email } = req.query;
    let user;
    if (id !== undefined) {
        user = await UserAction.findById(id);
    } else if (username !== undefined) {
        user = await UserAction.findByUsername(username);
    } else if (email !== undefined) {
        user = await UserAction.findByEmail(email);
    }
    if (user == undefined) {
        return res.status(HttpStatus.NO_CONTENT).end();
    }
    return res.status(HttpStatus.OK).json(user.output);
}

async function checkEmail(req, res) {
    const { email } = req.query;
    const user = await UserAction.findByEmail(email);
    return res.status(HttpStatus.OK).json({ emailUsed: user != undefined });
}

async function update(req, res) {
    return res.status(HttpStatus.OK).end();
}

async function whoAmI(req, res) {
    const { userId } = req.token;
    const user = await UserAction.findById(userId);
    if (user == null) {
        return res.status(HttpStatus.NOT_FOUND).json({
            error: 'User with this token doesnt exist',
        });
    }
    delete user.salt;
    delete user.passhash;
    return res.status(HttpStatus.OK).json(user);
}


module.exports = {
    create,
    retrieve,
    update,
    checkEmail,
    whoAmI,
};
