const HttpStatus = require('http-status-codes');
const UserAction = require('../actions/user');
const SessionAction = require('../actions/session');

const { checkPassword } = require('../../util');

async function create(req, res) {
    const { email, password } = req.body;
    const existingUser = await UserAction.findByEmail(email);
    if (existingUser == null || !checkPassword(password, existingUser.passhash, existingUser.salt)) {
        return res.status(HttpStatus.FORBIDDEN).json({
            error: 'Invalid credentials!',
        });
    }
    const session = SessionAction.create(existingUser.id);

    return res.status(HttpStatus.OK).set('Authorization', `Token ${session.token}`).json(session);
}

async function remove(req, res) {
    res.status(HttpStatus.OK).set('Authorization', '').json({ expiry: -1, token: '' });
}


module.exports = {
    create,
    remove,
};
