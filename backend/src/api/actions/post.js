const { client } = require('../../util/db');

async function create({ title, content, userId }) {
    const postResult = await client('posts').insert({ title, content, userId }).returning('*');
    return postResult[0];
}

async function findById(id) {
    return client('posts').select().where({ id });
}

async function findPostWithLikes(id) {
    return client.select(['u.username', 'u.firstName', 'u.lastName', 'p.*']).sum('pl.vote as totalVotes')
        .from(client.raw('posts AS p, post_likes as pl, users as u'))
        .whereRaw(`u.id = p."userId" and p.id = ${id} and p.id = pl."postId"`)
        .groupByRaw('p.id, u.id')
        .first();
}

async function findByUserId({ userId, limit, offset }) {
    const postsLimit = parseInt(limit || 20, 10);
    const postsOffset = parseInt(offset || 0, 10);
    let results = await client.select(['u.username', 'u.firstName', 'u.lastName', 'p.*']).sum('pl.vote as totalVotes')
        .from(client.raw('posts AS p, post_likes as pl, users as u'))
        .whereRaw(`u.id = p."userId" and p."userId" = ${userId} and p.id = pl."postId"`)
        .groupByRaw('p.id, u.id')
        .limit(postsLimit + 1)
        .offset(postsOffset);
    const resultsLength = results.length;
    // return only results that were initially queried
    results = results.length === 21 ? results.slice(0, 20) : results;
    return { results, hasMore: (postsLimit + 1) === resultsLength };
}

async function findMostLiked({ limit, offset }) {
    const postsLimit = parseInt(limit || 20, 10);
    const postsOffset = parseInt(offset || 0, 10);

    let results = await client.select(['p.*', 'pl.totalVotes'])
        .from(client.raw('posts AS p, "post_like_scores" as pl'))
        .whereRaw('p.id = pl."postId"')
        .orderByRaw('pl."totalVotes"')
        .limit(postsLimit + 1)
        .offset(postsOffset);
    const resultsLength = results.length;
    // return only results that were initially queried
    results = results.length === 21 ? results.slice(0, 20) : results;
    return { results, hasMore: (postsLimit + 1) === resultsLength };
}

async function votePost({ userId, postId, vote }) {
    return client('post_likes').insert({ userId, postId, vote });
}

async function findPostLike({ userId, postId }) {
    return client('post_likes').select().where({ userId, postId }).first();
}

async function deletePostLike({ userId, postId }) {
    return client('post_likes').where({ userId, postId }).del();
}

module.exports = {
    findById,
    findByUserId,
    create,
    findMostLiked,
    findPostWithLikes,
    findPostLike,
    deletePostLike,
    votePost,
};
