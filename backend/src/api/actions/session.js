const jwt = require('jsonwebtoken');
const { jwtConfig } = require('../../../config/constants');

function create(userId) {
    const exp = Math.floor(Date.now() / 1000) + ((60 * 60) * 24 * jwtConfig.expiry);
    const payload = {
        exp,
        userId,
    };
    return {
        token: jwt.sign(payload, jwtConfig.key),
        expiry: exp,
    };
}

module.exports = {
    create,
};
