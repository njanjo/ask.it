const { client } = require('../../util/db');

async function create(reply) {
    return client('replies').insert(reply).returning('*')[0];
}

async function findByPostId({ id, limit, offset }) {
    const postsLimit = parseInt(limit || 20, 10);
    const postsOffset = parseInt(offset || 0, 10);
    let results = await client('replies').select().where({ postId: id })
        .limit(postsLimit + 1)
        .offset(postsOffset);

    const resultsLength = results.length;
    // return only results that were initially queried
    results = results.length === 21 ? results.slice(0, 20) : results;
    return { results, hasMore: (postsLimit + 1) === resultsLength };
}

async function findById(id) {
    return client('replies').select().where({ id });
}

async function findByUserId(userId) {
    return client('replies').select().where({ userId });
}

async function findReplyWithLikes(replyId) {
    return client.select(['u.username', 'u.firstName', 'u.lastName', 'r.*']).sum('rl.vote as totalVotes')
        .from(client.raw('replies AS r, reply_likes as rl, users as u'))
        .whereRaw(`u.id = r."userId" and r.id = ${replyId} and rl."replyId" === r.id`)
        .groupByRaw('r.id, u.id');
}

async function findRepliesWithLikes({ postId, limit, offset }) {
    const repliesLimit = parseInt(limit || 20, 10);
    const repliesOffset = parseInt(offset || 0, 10);
    let results = await client.select(['u.username', 'u.firstName', 'u.lastName', 'r.*']).sum('rl.vote as totalVotes')
        .from(client.raw('replies AS r, reply_likes as rl, users as u'))
        .whereRaw(`u.id = r."userId" and r."postId" = ${postId} and r.id = rl."replyId"`)
        .groupByRaw('r.id, u.id')
        .limit(repliesLimit + 1)
        .offset(repliesOffset);
    const resultsLength = results.length;
    // return only results that were initially queried
    results = results.length === 21 ? results.slice(0, 20) : results;
    return { results, hasMore: (repliesLimit + 1) === resultsLength };
}

async function voteReply({ userId, replyId, vote }) {
    return client('reply_likes').insert({ userId, replyId, vote });
}

async function findReplyLike({ userId, replyId }) {
    return client('reply_likes').select().where({ userId, replyId });
}

module.exports = {
    findById,
    findByUserId,
    create,
    findByPostId,
    findRepliesWithLikes,
    voteReply,
    findReplyLike,
    findReplyWithLikes,
};
