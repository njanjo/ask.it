/* eslint-disable no-await-in-loop,eqeqeq */
const { sha512, genRandomString } = require('../../util');
const { client } = require('../../util/db');


async function create(params) {
    const user = {
        ...params,
    };
    const salt = genRandomString(16);
    const hash = sha512(params.password, salt);
    user.salt = hash.salt;
    user.passhash = hash.passwordHash;

    delete user.password;

    return client('users').insert(user).returning([
        'id',
        'firstName',
        'lastName',
        'username',
        'email',
    ]);
}

async function findByUsername(username) {
    return client.select()
        .from('users')
        .where('username', username).first();
}

async function findById(id) {
    return client.select()
        .from('users')
        .where('id', id).first();
}

async function findByEmail(email) {
    return client.select()
        .from('users')
        .where('email', email).first();
}

async function generateUsername(email) {
    let username = email.substring(0, email.indexOf('@'));
    let userExists = (await findByUsername(username)) != undefined;
    while (userExists) {
        username = `${username}1`;
        userExists = (await findByUsername(username)) != undefined;
    }
    return username;
}

module.exports = {
    findByUsername,
    findById,
    findByEmail,
    create,
    generateUsername,
};
