const validate = require('express-validation');

const router = require('express-promise-router')();
const auth = require('./auth');

const replyController = require('../api/controllers/reply');
const validation = require('./validation/index');

router.post('/vote/:id', auth.required, validate(validation.votePost), replyController.voteReply);
router.post('/', auth.required, validate(validation.createReply), replyController.createReply);
router.delete('/', auth.required, validate(validation.deleteReply), replyController.deleteReply);
router.get('/', auth.optional, validate(validation.retrieveReply), replyController.retrieveReply);
router.put('/', auth.required, validate(validation.updateReply), replyController.updateReply);
router.get('/all', auth.optional, validate(validation.allReplies), replyController.allReplies);
router.get('/from-post/:id', auth.optional, replyController.fromPost);

module.exports = router;
