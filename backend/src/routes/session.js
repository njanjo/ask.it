const validate = require('express-validation');

const router = require('express-promise-router')();
const auth = require('./auth');

const sessionController = require('../api/controllers/session');
const validation = require('./validation/index');

router.post('/', auth.optional, validate(validation.createSession), sessionController.create);
router.delete('/', auth.optional, sessionController.remove);

module.exports = router;
