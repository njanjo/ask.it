const validate = require('express-validation');

const router = require('express-promise-router')();
const auth = require('./auth');

const userController = require('../api/controllers/user');
const validation = require('./validation/index');

router.post('/', auth.optional, validate(validation.createUser), userController.create);
router.get('/', auth.required, userController.retrieve);
router.put('/', auth.required, userController.update);
router.get('/check-email', auth.optional, validate(validation.checkEmail), userController.checkEmail);
router.get('/whoami', auth.required, userController.whoAmI);

module.exports = router;
