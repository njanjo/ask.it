const Joi = require('joi');

module.exports = {
    body: {
        firstName: Joi.string(),
        lastName: Joi.string(),
        email: Joi.string().email().required(),
        password: Joi.string().required(), // TODO add password constraints
    },
};
