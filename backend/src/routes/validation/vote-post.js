const Joi = require('joi');

module.exports = {
    body: {
        vote: Joi.number().min(-1).max(1).required(),
    },
};
