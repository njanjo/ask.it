const Joi = require('joi');

module.exports = {
    headers: {
        Authorization: Joi.string(),
    },
};
