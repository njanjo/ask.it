const Joi = require('joi');

module.exports = {
    body: {
        postId: Joi.number().required(),
        content: Joi.string().required(),
    },
};
