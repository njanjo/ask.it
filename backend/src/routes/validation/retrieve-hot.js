const Joi = require('joi');

module.exports = {
    query: {
        limit: Joi.number(),
        offset: Joi.number(),
    },
};
