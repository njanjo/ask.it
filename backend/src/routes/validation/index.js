const createUserValidation = require('./create-user');
const checkEmailValidation = require('./check-email');
const createSessionValidation = require('./create-session');
const retrieveSessionValidation = require('./create-session');
const createReplyValidation = require('./create-reply');
const createPostValidation = require('./create-post');
const deleteReplyValidation = require('./delete-reply');
const deletePostValidation = require('./delete-post');
const updateReplyValidation = require('./update-reply');
const updatePostValidation = require('./update-post');
const retrieveReplyValidation = require('./retrieve-reply');
const retrievePostValidation = require('./retrieve-post');
const allRepliesValidation = require('./all-replies');
const allPostsValidation = require('./all-posts');
const votePostValidation = require('./vote-post');

module.exports = {
    createUser: createUserValidation,
    checkEmail: checkEmailValidation,
    createSession: createSessionValidation,
    retrieveSession: retrieveSessionValidation,
    createReply: createReplyValidation,
    createPost: createPostValidation,
    deleteReply: deleteReplyValidation,
    deletePost: deletePostValidation,
    retrievePost: retrievePostValidation,
    retrieveReply: retrieveReplyValidation,
    updateReply: updateReplyValidation,
    updatePost: updatePostValidation,
    allReplies: allRepliesValidation,
    allPosts: allPostsValidation,
    votePost: votePostValidation,
};
