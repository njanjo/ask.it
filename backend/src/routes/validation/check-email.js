const Joi = require('joi');

module.exports = {
    query: {
        email: Joi.string().email().required(),
    },
};
