const Joi = require('joi');

module.exports = {
    query: {
        id: Joi.number().required(),
    },
};
