const Joi = require('joi');

module.exports = {
    body: {
        title: Joi.string().required(),
        content: Joi.string().required(),
    },
};
