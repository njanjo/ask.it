const expressJwt = require('express-jwt');
const { jwtConfig } = require('../../config/constants');

const getTokenFromHeaders = req => {
    const { headers: { authorization } } = req;

    if (authorization && authorization.split(' ')[0] === 'Token') {
        return authorization.split(' ')[1];
    }
    return null;
};

module.exports = {
    required: expressJwt({
        secret: jwtConfig.key,
        userProperty: 'token',
        getToken: getTokenFromHeaders,
    }),
    optional: expressJwt({
        secret: jwtConfig.key,
        userProperty: 'token',
        getToken: getTokenFromHeaders,
        credentialsRequired: false,
    }),
};
