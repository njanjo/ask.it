const validate = require('express-validation');

const router = require('express-promise-router')();
const auth = require('./auth');

const postController = require('../api/controllers/post');
const validation = require('./validation/index');

router.post('/vote/:id', auth.required, validate(validation.votePost), postController.votePost);
router.post('/', auth.required, validate(validation.createPost), postController.createPost);
router.delete('/', auth.required, validate(validation.deletePost), postController.deletePost);
router.get('/', auth.optional, validate(validation.retrievePost), postController.retrievePost);
router.get('/with-replies/:id', auth.optional, postController.retrievePostAndReplies);
router.get('/hot', auth.optional, postController.hot);
router.get('/all', auth.optional, validate(validation.allPosts), postController.allPosts);

module.exports = router;
