const knex = require('knex');
const { postgresConfig } = require('../../config/constants');

const client = knex(postgresConfig);


module.exports = {
    client,
};
