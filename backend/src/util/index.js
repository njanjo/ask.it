const crypto = require('crypto');

const genRandomString = length => crypto.randomBytes(Math.ceil(length / 2))
    .toString('hex')
    .slice(0, length);

const sha512 = (password, salt) => {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt,
        passwordHash: value,
    };
};

const checkPassword = (password, passhash, salt) => sha512(password, salt).passwordHash === passhash;


module.exports = {
    genRandomString,
    sha512,
    checkPassword,
};
